__author__ = 'Mark Provenzano'

import os

file = 'sec1.txt'

lines = []
unwanted = ['&', '/', '(', ')', '-', ' ', '\\', '.']

with open(file) as f:
    line = f.readline()
    while line:
        if not line.isupper():
            line = line.replace('.', '').replace(',', '.')
            lines.append(line)
        line = f.readline()

nf_name = "{f}_cleaned.csv".format(f=file[:file.index('.')])

if os.path.isfile(nf_name):
    print("removing {}".format(nf_name))
    os.remove(nf_name)

with open(nf_name, "w+") as new_file:
    print("creating file {}".format(nf_name))
    for l in lines:
        idx = 0
        for c in l:
            if not c.isalpha() and c not in unwanted:
                break
            idx += 1

        l1 = str(l[:idx]).strip()
        l2 = l[idx:].replace(' ', ',').strip()
        lf = str("{},{}\n".format(l1, l2)).split(',')
        if not len(lf) < 4:
            if lf[4] == '*':
                lf.remove('*')

        lf = str(lf).replace('[', '').replace(']', '').replace('\'', '')
        new_file.write(str(lf) + "\n")

print("{} was created".format(nf_name))